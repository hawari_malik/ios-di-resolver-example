//
//  Inject.swift
//  DI_test
//
//  Created by IT Resource Center on 11/10/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import Foundation
import Resolver

@propertyWrapper
struct Inject<Service> {
    private var service: Service?
    public var container: Resolver?
    public var name: String?
    public var wrappedValue: Service {
        mutating get {
            if service == nil {
                service = (container ?? Resolver.root).resolve(
                    Service.self,
                    name: name
                )
            }
            return service!
        }
        mutating set {
            service = newValue
        }
    }
    public var projectedValue: Inject<Service> {
        get {
            return self
        }
        mutating set {
            self = newValue
        }
    }
}
