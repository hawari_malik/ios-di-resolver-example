//
//  ServiceRegistration.swift
//  DI_test
//
//  Created by IT Resource Center on 11/10/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import Foundation
import Resolver

extension Resolver : ResolverRegistering {
    public static func registerAllServices() {
        register { DashboardVMRed() as DashboardVM }
        register { ProfileVMRandom() as ProfileVM }
        register { LoginVMBypass() as LoginVM }
    }
}

