//
//  DashboardVMBlue.swift
//  DI_test
//
//  Created by IT Resource Center on 07/10/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import Foundation
import Foundation
import UIKit

class DashboardVMBlue : DashboardVM {
    func getColor() -> UIColor {
        return UIColor.blue
    }
    func getTexts() -> [String] {
        return [
        "the loading above is blue colored",
        "last vm",
        "i hope you don't notice the text",
        "because even i don't know what i write"
        ]
    }
}
