//
//  DashboardVC.swift
//  DI_test
//
//  Created by IT Resource Center on 07/10/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import UIKit

class DashboardVC: UIViewController {
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var vstackTextHolder: UIStackView!
    
    @Inject private var vm: DashboardVM
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Dashboard"
        self.setUI()
    }
    
    private func setUI(){
        self.activityIndicator.color = self.vm.getColor()
        for s in self.vm.getTexts() {
            let lbl = UILabel()
            lbl.text = s
            vstackTextHolder.addArrangedSubview(lbl)
        }
    }

    @IBAction func btnProfileDidPress(_ sender: UIButton) {
        self.navigationController?.pushViewController(ProfileVC(), animated: true)
    }
    
}
