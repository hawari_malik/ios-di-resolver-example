//
//  LoginVMLength.swift
//  DI_test
//
//  Created by IT Resource Center on 07/10/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import Foundation

class LoginVMLength : LoginVM {
    func validate(username u: String?, password p: String?) -> String? {
        let us = u ?? ""
        let ps = p ?? ""
        if us.count < 5 {
            return "username < 5"
        }
        if ps.count < 5 {
            return "password < 5"
        }
        return nil
    }
}
