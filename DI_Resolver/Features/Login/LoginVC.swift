//
//  LoginVC.swift
//  DI_test
//
//  Created by IT Resource Center on 07/10/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import UIKit
import Resolver

class LoginVC: UIViewController {
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var lblError: UILabel!
    
    private var vm: LoginVM = Resolver.resolve()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        txtUsername.text = ""
        txtPassword.text = ""
        lblError.text = ""
    }

    @IBAction func btnLoginDidPress(_ sender: UIButton) {
        if let err = vm.validate(username: txtUsername.text, password: txtPassword.text){
            lblError.text = err
            lblError.textColor = .systemRed
            return
        }
        lblError.text = "login success"
        lblError.textColor = .systemGreen
        self.navigationController?.pushViewController(DashboardVC(), animated: true)
    }
    
}
