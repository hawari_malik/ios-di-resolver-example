//
//  ProfileVMRandom.swift
//  DI_test
//
//  Created by IT Resource Center on 10/10/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import Foundation
import UIKit

class ProfileVMRandom : ProfileVM {
    func getName() -> String {
        Misc.stringGen(length:20, charset:"abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ")
    }
    
    func getID() -> String {
        Misc.stringGen(length:10, charset:"1234567890")
    }
    
    func getColor() -> UIColor {
        Misc.colorGen()
    }
    
}
