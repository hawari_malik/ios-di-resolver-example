//
//  ProfileVMStandard.swift
//  DI_test
//
//  Created by IT Resource Center on 10/10/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import Foundation
import UIKit

class ProfileVMStandard : ProfileVM {
    func getName() -> String {
        "Hawari Al Malik"
    }
    
    func getID() -> String {
        "2201804240"
    }
    
    func getColor() -> UIColor {
        UIColor.green
    }
    
    
}
