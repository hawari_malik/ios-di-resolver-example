//
//  ProfileVC.swift
//  DI_test
//
//  Created by IT Resource Center on 10/10/19.
//  Copyright © 2019 hawari_am. All rights reserved.
//

import UIKit

class ProfileVC : UIViewController {
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblId: UILabel!
    
    @Inject private var vm: ProfileVM
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Profile"
        imgProfile.backgroundColor = vm.getColor()
        lblName.text = vm.getName()
        lblId.text = vm.getID()
    }

}
